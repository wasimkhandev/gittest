﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GitTest.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            int a = 1;
            int b = 2;
            int c = 3;
            int d = 4;
            int sum = mySum(a, b);
            return View();
        }

        private int mySum(int a, int b)
        {
            return a + b;
        }

        public ActionResult About()
        {
            int printSum = 5 + 8;
            ViewBag.Message = printSum;

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}